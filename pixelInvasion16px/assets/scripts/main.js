var i = 1;
var ibis = 255;
var j = 0;
var interval = 100;
var glifSize = 16;

var windowW = window.innerWidth;
var windowY = window.innerHeight;
var origin = posX;
var propagation = [glifSize, -glifSize, 0];
var posX = windowW / 2 - 8;
var posY = windowY / 2 - 8;
var box = document.getElementById('propagation');
var prevPos = [];
var newpos = []
var newPosX, newPosY, randX, randY, glif;


function storeCoordinate(xVal, yVal, array) {
  array.push({ x: xVal, y: yVal });
}

storeCoordinate(posX, posY, prevPos);


function setPosition() {
  randX = propagation[randNb(0, propagation.length)];
  randY = propagation[randNb(0, propagation.length)];
  var randPrevPos = randNb(0, prevPos.length);
  newPosX = prevPos[randPrevPos].x + randX;
  newPosY = prevPos[randPrevPos].y + randY;

}


var loop = setInterval(() => {

  glif = document.createElement('div');


  glif.style.left = posX + 'px';
  glif.style.top = posY + 'px';

  if (i < 255) {
    glif.style.background = 'rgb(' + i + ',' + i + ',' + i + ')';
    i = i + 1;
    ibis = 255;
  } else if (ibis < 1) {
    glif.style.background = 'rgb(' + i + ',' + i + ',' + i + ')';
    i = 1;
    i = i + 1;
  } else {
    glif.style.background = 'rgb(' + ibis + ',' + ibis + ',' + ibis + ')';
    ibis = ibis - 1

  }

  setPosition();
  var nope = true;
  while (nope == true) {
    const check = ([x, y]) => prevPos.some(o => o.x === x && o.y === y);
    if (check([newPosX, newPosY]) == true) {
      setPosition();
    } else {
      nope = false;
    }
  }

  posX = newPosX;
  posY = newPosY;

  storeCoordinate(posX, posY, prevPos);

  box.appendChild(glif);

  (j < 4000) ? j++ : clearInterval(loop);

}, interval);

function randNb(min, max) {
  return Math.floor((Math.random() * max) + min)
}