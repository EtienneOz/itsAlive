var i = 1;
var ibis = 255;
var j = 1;

var windowW = window.innerWidth;
var windowY = window.innerHeight;
var propagation = [1, -1, 0];
var posX = windowW / 2 - 8;
var posY = windowY / 2 - 8;
var box = document.getElementById('propagation');
var prevPosX = [];
var prevPosY = [];
var newposX, newposY, randX, randY, glif;

setInterval(() => {

  glif = document.createElement('div');
  if (j > 1) {
    randY = randNb(3, 1);
    newposY = posY + propagation[randY - 1]
    randX = randNb(3, 1);
    newposX = posX + propagation[randX - 1];

    posX = newposX;
    posY = newposY;
  }

  if (i < 255) {
    glif.style.background = 'rgb(' + i + ',' + i + ',' + i + ')';
    i = i + 1;
    ibis = 255;
  } else if (ibis < 1) {
    glif.style.background = 'rgb(' + i + ',' + i + ',' + i + ')';
    i = 1;
    i = i + 1;
  } else {
    glif.style.background = 'rgb(' + ibis + ',' + ibis + ',' + ibis + ')';
    ibis = ibis - 1

  }



  glif.style.left = posX + 'px';
  glif.style.top = posY + 'px';

  box.appendChild(glif);

  j++


}, 150);

function randNb(max, min) {
  return Math.floor((Math.random() * max) + min)
}